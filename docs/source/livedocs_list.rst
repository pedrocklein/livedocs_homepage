LiveDocs List
================

In the CRC1456 we have two different types of Livedocs: Educational LiveDocs and Reproduction LiveDocs:


Educational LiveDocs
--------------------

Educational LiveDocs are intended to explore specific topics of interest permeating the CRC 1456 projects. This type of LiveDocs can refer, for instance, to specific topics that are approached in one or more  CRC 1456 publications, presenting users with a ludic overview of such topics, and explaining findings on  publications, allowing not only the replication of findings but also the experimentation with presented data.  Moreover, such LiveDocs can also refer to tools created to facilitate research on CRC 1456 topics, presenting  users with a  walkthrough on how to use these tools, examples and other tutorials.

.. toctree::
   :maxdepth: 2

   guided_jupyter_tour_ot
   c04_helio

Reproduction LiveDocs
---------------------

Reproduction LiveDocs are intended to explore the findings of specific CRC 1456 publications, providing users with the scripts and datasets associated with the results shown in such publications. This allows the assessment of findings with easiness since the presented development environments are pre-built with all the requirements, thus demanding little to no work from the users' side when it comes to setting up such environments.

.. toctree::
   :maxdepth: 2

   granule_cell_analysis
   qmri_nist