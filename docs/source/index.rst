.. LiveDocsHomepage documentation master file, created by
   sphinx-quickstart on Wed Nov 29 09:04:46 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: assets/index/LiveDocsLogo.png
   :width: 33%
   :align: center

CRC 1456 LiveDocs
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

To enforce open and reproducible research, the CRC 1456 projects can provide the results from their analysis 
in the form of showcases, in LiveDocs. LiveDocs are reproducible environments designed based on CRC 1456 projects'
repositories, with the intent of providing the general public with access to the code and data used in projects' 
analysis, thus allowing the reproduction of reported results.

The main concept behind LiveDocs is to provide everything needed for the general audience to be able to interact 
with your code, either for scientific divulgation purposes or to spread awareness on specific themes (but not 
limited to these two cases). The Livedocs are provided in a ready-to-run format, which allows your code to be 
executed anywhere.

As for the involved technologies, currently, the LiveDocs can be delivered in one or more of the following formats:

- Binderized applications running either in the `Private <http://c111-004.cloud.gwdg.de:30901/>`_ or `Public <http://c109-005.cloud.gwdg.de:30901/>`_ CRC 1456 Binderhub Servers in:

   - Jupyter Lab server

   - Jupyter Notebook (Classic) server

   - Jupyter server with `Voilá <https://voila-gallery.org/>`_

- Applications that run locally in the browser, using JupyterLite

- Static HTML generated from Jupyter Scripts

- Containerized applications for `Docker <https://www.docker.com/>`_

LiveDocs Template
----------------- 

At the CRC 1456 Gitlab, the INF project provides a `template <https://gitlab.gwdg.de/crc1456/livedocs/livedocs_template>`_ for you to create your LiveDocs in the GWDG Gitlab. 
You can download this repository and start working on it. More information can be found in its `readme <https://gitlab.gwdg.de/crc1456/livedocs/livedocs_template/-/blob/master/README.md>`_ file.

If you are a CRC 1456 member and have questions on how to create a LiveDoc of your own, or about any specificities 
concerning the LiveDocs, please contact us.

..
   Readme file
   -----------
   .. mdinclude:: ../../README.md
..
         
Contents
--------
.. toctree::
   :maxdepth: 3

   known_issues
   livedocs_list   
..
   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
..